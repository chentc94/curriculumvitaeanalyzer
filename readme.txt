---------------Running class file-----------------------
To run the .class file, firstly compile all the java file via Eclipse 
Go to the folder that contains the main.class file.
Type commandline java main argument1 argument2 argument3

For applications the command should be
java main resume_path userId jobId applicationId

For job description the command should be,
java main resume_path jobId  


---------------Compilation into JAR file-----------------
Right click on the project and select Export in Eclipse.
Choose the Runnable Jar file in Java, click next.
Choose the right launch configuration to run the project.
Choose the export destination.
Choose the Package required libraries into generated JAR.



--------------------Running JAR file---------------------
To run the jar file, go to the folder that contains the jar file.
then type commandline java - jar parser.jar argument1 argument2 argument3

For applications the command should be
java - jar parser.jar resume_path userId jobId applicationId

For job description the command should be,
java - jar parser.jar jobdescription jobId  